# Vagrant FDTE-DSD

## Máquina Virtual para desenvolvimento Web da FDTE-DSD

Este repositório contém scripts que permitem a criação de uma Máquina Virtual
com tudo necessário para desenvolver os projetos da FDTE com o menor esforço
do desenvolvedor. Ela inclui um ambiente Linux configurado com as principais
ferramentas usadas nos projetos dentro da FDTE.

Este projeto inclui:

- [Arch Linux](https://www.archlinux.org/)
- [MATE Desktop](https://mate-desktop.org) pré-configurado
- Navegadores: Chromium (Google Chrome) e Firefox
- Editores de textos diversos: Atom, Emacs, Sublime Text 3, (Neo)Vim
- Console ZSH configurado
- Ambientes de desenvolvimento:
	* Python (pip e virtualenv)
	* Ruby ([rbenv](https://github.com/rbenv/rbenv))
	* Node (incluindo npm, glunt-cli, gulp, bower, etc.)
- PostgresSQL instalado
- Configurações padrões decentes

Caso esteja faltando alguma coisa faça uma sugestão ou Pull Request!

## Instalação

É necessário instalar o [Vagrant](https://www.vagrantup.com/downloads.html) e o
[VirtualBox](https://www.virtualbox.org/) na sua máquina.

Depois da instalação, abra o `Prompt de Comando` na pasta contendo os arquivos
desse repositório (no Windows: abra essa pasta no `Windows Explorer`, botão
direito e *Abrir janela de comando aqui*) e digite:

```
> vagrant up
```

E aguarde. O processo de construção da máquina virtual deve demorar algumas
dezenas de minutos, dependendo da velocidade da sua internet. Uma janela do
VirtualBox irá abrir durante o processo, não feche ela.

Ao final, a máquina virtual será reiniciada automaticamente e você deve se
deparar com a tela de login. Logue-se usando:

- Login: vagrant
- Senha: vagarnt

Uma vez com a máquina virtual montada, não é necessário mais usar o `vagrant`
para iniciá-la, podemos usar a própria interface do VirtualBox.

## Se familiariazando com o Arch Linux

O Arch Linux é conhecido como uma distro para usuários Linux mais avançados,
porém a parte de instalação que é uma das mais complicadas é feito de forma
automática pelos scripts desse repositório. De resto, a distro é simples de
utilizar, tem uma excelente documentação na sua [Wiki](https://wiki.archlinux.org/)
(famosa inclusive por ajudar em problemas que usuários tem em outras distros)
e com pacotes sempre atualizados que é essencial para um desenvolvedor
moderno.

Alguns comandos básicos:

- Atualizar o sistema

        pacaur -Syu

- Procurar um pacote

        pacaur -Ss pacote

- Instalar um pacote

        pacaur -S pacote

## Compartilhando arquivos com a Máquina Virtual

Qualquer arquivo que estiver dentro da pasta atual vai ser compartilhado com
a máquina virtual dentro do diretório `/vagrant`. É possível editar o
`Vagrantfile` para adicionar mais pastas de compartilhamento também, caso
seja necessário.

## Rede privada com a Máquina Virtual

A máquina virtual está configurada com uma rede privada com o host no IP
`192.168.33.10`, o que significa que você pode acessar os serviços rodando na
máquina virtual sem problemas.
